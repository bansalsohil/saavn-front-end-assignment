# Guidelines
==========================================================

## TimeSheet
| ---------------------- | ---------- |
| HTML Files             | 00h 19min  |
| Form CSS               | 01h 09min  |
| CSS Changes For signup | 00h 29min  |
| Form HTML Validations  | 00h 31min  |
| Form Validation JS     | 01h 15min  |
| Json Display           | 00h 21min  |
| Json template          | 00h 48min  |
| Background             | 00h 20min  |
| CSS3 Transitions       | 01h 22min  |
| Tracks CSS             | 01h 35min  |
| ---------------------- | ---------- |
| Total                  | 08h  09min |
| ---------------------- | ---------- |

## Libraries Used
- jQuery
- Adobe Typekit: Lato-Medium was not available on Google Fonts
- LastFM API

## Images Used
- Chevron Down for fixing 'select' styling using CSS


# Project
===========================================================

## Development Environment
- PugJs is used for HTML template. It makes code look very clean and enforces good xml practices
- Sass is used as CSS preprocessor. Indent version is used for keeping code clean and efficient
- Folder contains src files with productions files in 'Build' folder

## Tasks Completed

### SignUp form
- HTML5 form validations with check for pattern and required fields
- Simple JS Validation to check validity and stop submission
- Styling based on pseudo CSS3 classes for invalid and focused fields

### Get current Top 10
- API Call to LastFM using Ajax to get Json result
- Responsive styling to adjust for smaller screen sizes

### CSS3 Transitions
- Hide Form after submissions
- Show results with a Slide-In effect

## Tasks not completed
- Confirmation email using PHP
- Page transition to a loading state


# Closing Notes
===========================================================

## Should have been done better

### Form Validation and errors
- Specific error notes for each field
- Show error on blur and focusOut
- Fix placeholder styling for select field
- On mobile fix bottom background scroll while opening keyboard

### CSS3 Transitions
- Play around with timing and curve of various transitions to create smooth transition


## Things I should have added

### Input screen for query
- Provide query type: top artists, top albums, top tracks
- Provide an option to load more results
- Play link to stream the audio

### User experience
- Faux loading screen to make user think, something interesting will happen



