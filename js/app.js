$( "#signUp" ).submit(function( event ) {
	event.preventDefault();	
    
    if(!validateForm($(this))){
        return;
    }
    
	// Ajax Request for Getting Content from LastFM API
	$.ajax({
		dataType: "json",
		url: 'http://ws.audioscrobbler.com/2.0/',
		data: {
			method	: "chart.gettoptracks",
			limit	: "10",
			api_key	: "eef68a8d6c54c75b973dffcd544e7854",
			format	: "json"
		},
		success: function( result ){
			var topTracks = result.tracks.track;
			var track = [];

			track +=    '<section class="welcome">' +
						    '<h1 class="welcome__title">Boom.</h1>' +
						    '<p class="welcome__subtitle">' + topTracks[0].artist.name + ' is #1.</p>' +
					    '</section>';

			// Loop over each track
			for ( i = 0; i < topTracks.length; i++){	
				track +=    '<article class="track">' +
								'<img src=' + topTracks[i].image[3]['#text'] + '">' +
								'<p class="rank">' + (i+1) + "</p>" +
								'<div class="track__details">' +
									'<div class="track__name-artist">' +
										'<h2>' + topTracks[i].name + '</h2>' +
										'<p>' + topTracks[i].artist.name + '</p>' +
									'</div>' +
                                    '<div class="track__play">' +
                                        '<p class="track__playcount">' + topTracks[i].playcount + '</p>' +
                                        '<p>Plays</p>' +
                                    '</div>' +
								'</div>' +
							'</article>';
			}
			$("#tracks").html(track);
			$(".signup").addClass('hidden'); // Hide Signup Form
			$("body").addClass('bg_tracks'); // Change Body class
		}
	});
});

// A very simple Validate Form
function validateForm(form){
	var allInputs = form.find("input, select");
    var result = true;
    
	allInputs.each(function() {
		if($(this)[0].validity.valid == false){
            $(this)[0].after('<p class="input_error">Please correctly enter details</p>');
            result = false;
            return result;
        }
    });
    return result;
}